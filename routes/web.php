<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');


Route::get('/', 'FrontendController@index');
Route::get('/{slug}', 'FrontendController@page');
/*
Route::get('/blog', 'FrontendController@page')->name('blog');
Route::get('/about', 'FrontendController@page')->name('about');
Route::get('/contact', 'FrontendController@page')->name('contact');
 */

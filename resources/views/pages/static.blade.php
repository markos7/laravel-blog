@extends('layouts.app')
@section('content')


<h1>{{$page->title}}</h1>

<div class="card bg-light text-dark">
<div class="card-body">{{$page->content}}</div>
  </div>
@endsection

